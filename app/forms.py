# -*- coding: utf-8 -*-
"""
Created on Fri May 25 13:42:37 2018

@author: User
"""

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField, IntegerField, HiddenField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from app.models import User
from flask_login import current_user



class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')
    
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')

'''class FileSelect(FlaskForm):
    model = StringField('Model Path')
    submit_files_select = SubmitField('Select')

def available_models():
    return ModelFiles.query.filter_by(user_id=current_user.id).order_by(ModelFiles.timestamp.desc()).all()

class ModelSelect(FlaskForm):
    model_list = QuerySelectField('Your models', query_factory=available_models, get_label='model_name')
    submit_model_select = SubmitField('Change model')

class CurrentModel(FlaskForm):
    model_list = QuerySelectField(get_label='model_name')
    submit_current_model = SubmitField('Change model')

class RoomSelect(FlaskForm):
    room_list = QuerySelectField(get_label='room_number')
    submit_room = SubmitField('Select room')

class PopClass(FlaskForm):
    language = SelectField('options', choices=[('opp1', '1'), ('opp2', '2')])
    submit_pop_class = SubmitField('Load classifications')

class ObjectSelect(FlaskForm):
    object_list = QuerySelectField(get_label='object_name')
    submit_object = SubmitField('view information')

class PropSelect(FlaskForm):
    property_list = QuerySelectField(get_label='property_set_name')
    submit_property = SubmitField('view properties')'''