# -*- coding: utf-8 -*-
"""
Created on Fri May 25 13:15:29 2018

@author: User
"""

from flask import render_template, flash, redirect, url_for, request, session, make_response
from flask.json import jsonify
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User
from app.email import send_password_reset_email
from app import app, db
from app.forms import LoginForm, RegistrationForm
from app.forms import ResetPasswordRequestForm, ResetPasswordForm
from operator import itemgetter
from werkzeug.urls import url_parse


path = ""


@app.route('/')
@app.route('/index')
def index():

    return render_template('index.html', title='Home')



@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    session.pop('space_id', None)
    session.pop('object_id', None)
    session.pop('property_set_id', None)
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html',
                           title='Reset Password', form=form)


@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)

'''@app.route('/select_model', methods=['GET', 'POST'])
@login_required
def select_model():
    form = FileSelect()
    form1 = CurrentModel()
    form2 = PopClass()
    models = ModelFiles.query.filter_by(user_id=current_user.id).order_by(ModelFiles.timestamp.desc()).all()
     
    if form.submit_files_select.data and form.validate_on_submit():
        previous_model = ModelFiles.query.filter_by(current_model=True).first()
        if previous_model:
            previous_model.current_model = False
        model = ModelFiles(model_path=form.model.data, user_id=current_user.id, model_name=file_manipulation.path_leaf(form.model.data)
            , current_model=True)
        db.session.add(model)
        db.session.commit()
        flash('model added {}'.format(form.model.data))
        model_entry = ModelFiles.query.filter_by(model_path=form.model.data).order_by(ModelFiles.timestamp.desc()).first()
        space_data = spaceTableDisplay1.space(form.model.data)
        print ('STEP 1')
        for space in space_data:
            #geom = space[7] - accepts data when .geometry
            space_area = SpaceArea(room_number=space[0], room_name=space[1], floor_area=space[2],
                bb_height=space[3], room_perimeter=space[4], room_classification=space[5], model_files_id=model_entry.id, 
                    guid=space[6], geom=space[7]) #database does not accept this: geometry=space[7]
            db.session.add(space_area)
            db.session.commit()
            #database_utils.class_find(space[5], space[6], current_user)
            class_lookup = Classifications.query.filter_by(class_ref=space[5]).first()
            object_lookup = SpaceArea.query.filter_by(guid=space[6], model_files_id=model_entry.id).first()
            #print (class_lookup, object_lookup)
            if class_lookup:
                class_match = ClassSpace(space_id=object_lookup.id, class_id=class_lookup.id, user_id=current_user.id) # No class GUID class_guid
                db.session.add(class_match)
                db.session.commit()
        
        objects = ObjectsWithClassification.classifications(form.model.data)
        ifc_model = ifcopenshell.open(form.model.data)
        #spaces = SpaceArea.query.filter_by(model_path=form.model.data).all()
        model_select = ModelFiles.query.filter_by(current_model=True, user_id=current_user.id).first()
        spaces = SpaceArea.query.filter_by(model_files_id=model_select.id).all()
        print ('STEP 2')
        for item in objects:
            if ('Type' or 'IfcSpace' or 'IfcProject' or 'IfcSite' or 'IfcFloor' or 'IfcBuilding') in ifc_model[item[3]].is_a():
                pass
            else: 
                try:
                    #g = geometry_info.create_shape(ifc_model[item[3]]) # need to convert to .geometry for db
                    g = geometry_info.create_shape_geom(ifc_model[item[3]])
                    #com = geometry_info.calc_centre_mass(g)
                    com = geometry_info.calc_com_geom(g)

                    #_, com = geometry_info.object_geom(ifc_model[item[3]])
                    #cofm = str(com)
                    for space in spaces:
                        #print ('space searched')
                        #print (space.geometry)
                        #l = geometry_info.create_shape(ifc_model[space.guid])
                        l = space.geom
                        #t = geometry_info.point_inside(space.geometry, com)
                        t = geometry_info.point_inside(l, com)
                        #print ('spaced search', space.room_number)
                        if t[0]:
                            space_match = space.id
                            break
                        else:
                            space_match = None

                except:
                    space_match = None
                if space_match:
                    object_data = Objects(object_name=item[4], object_desc=item[5], object_guid=item[3], model_files_id=model_entry.id, 
                        space_area_id=space_match, centre_of_mass=com, geom=g)
                else:
                    object_data = Objects(object_name=item[4], object_desc=item[5], object_guid=item[3], model_files_id=model_entry.id, 
                        centre_of_mass=com, geom=g)
                db.session.add(object_data)
                db.session.commit()
            
            class_lookup = Classifications.query.filter_by(class_ref=item[0]).first()
            object_lookup = Objects.query.filter_by(object_guid=item[3], model_files_id=model_entry.id).first()
            if class_lookup:
                #print ('object class match', class_lookup.class_ref)
                class_match = ClassObject(objects_id=object_lookup.id, class_id=class_lookup.id, class_guid=item[1], user_id=current_user.id)
                db.session.add(class_match)
                db.session.commit()
 

        all_psets = return_info.psets(ifc_model)
        print ('STEP 3')
        for i in all_psets: #i[0] = object, i[1][0] = name of property set, i[1][1] = property set guid, i[1][3] = property set individual values
            #print (i)
            object_match = Objects.query.filter_by(object_guid=i[0][0][0], model_files_id=model_entry.id).first()
            if object_match:
                if i[1][2]:
                    if object_match.space_area_id:
                        pset = PropertySet(property_set_name=i[1][0], property_set_desc=i[1][2], property_set_guid=i[1][1], 
                            object_id=object_match.id, space_area_id=object_match.space_area_id, model_files_id=model_entry.id)
                    else:
                        pset = PropertySet(property_set_name=i[1][0], property_set_desc=i[1][2], property_set_guid=i[1][1], 
                            object_id=object_match.id, model_files_id=model_entry.id)
                else:
                    if object_match.space_area_id:
                        pset = PropertySet(property_set_name=i[1][0], property_set_guid=i[1][1], object_id=object_match.id, 
                            space_area_id=object_match.space_area_id, model_files_id=model_entry.id)
                    else:
                        pset = PropertySet(property_set_name=i[1][0], property_set_guid=i[1][1], object_id=object_match.id, 
                            model_files_id=model_entry.id)
                db.session.add(pset)
                db.session.commit()
                pset_match = PropertySet.query.filter_by(property_set_guid=i[1][1], model_files_id=model_entry.id).first()
                for p in i[1][3]:
                    if p:
                        if p[1]:
                            if p[2]:
                                print ('match 1 -', p)
                                single_value = Property(property_name=p[0], property_desc=p[1], property_value=p[2].wrappedValue, 
                                    property_type=p[2].is_a(), property_set_id=pset_match.id)
                            else:
                                print ('match 2 -', p)
                                single_value = Property(property_name=p[0], property_desc=p[1], property_set_id=pset_match.id)
                        else:
                            if p[2]:
                                print ('match 3 -', p)
                                single_value = Property(property_name=p[0], property_value=p[2].wrappedValue, property_type=p[2].is_a(), property_set_id=pset_match.id)
                            else:
                                print ('match 4 -', p)
                                single_value = Property(property_name=p[0], property_set_id=pset_match.id)
                        db.session.add(single_value)
                        db.session.commit()

    all_models = ModelFiles.query.filter_by(user_id=current_user.id).order_by(ModelFiles.timestamp.desc()).all()
    if models:
        form1.model_list.query = models
        if form1.submit_current_model.data and form1.validate_on_submit():
            previous_model = ModelFiles.query.filter_by(current_model=True).first()
            flash ('current model {}'.format(form1.model_list.data.model_name))
            if previous_model:
                previous_model.current_model = False
                db.session.commit()
            model_entry = form1.model_list.data
            new_current = ModelFiles.query.filter_by(id=model_entry.id).first()
            new_current.current_model = True
            db.session.commit()
        return render_template('select_model.html', title='Model Select', form=form, form1=form1, form2=form2, all_models=all_models)  

    return render_template('select_model.html', title='Model Select', form=form, form2=form2, all_models=all_models)  
  

@app.route('/select/', methods=['GET', 'POST'])
@login_required
def select():
    form3 = RoomSelect()
    form4 = ObjectSelect()
    form5 = PropSelect()
    new_items = []
    model_select = ModelFiles.query.filter_by(current_model=True, user_id=current_user.id).first()
    form3.room_list.query = SpaceArea.query.filter_by(model_files_id=model_select.id).order_by(SpaceArea.room_number.desc()).all()
    if 'space_id' in session:
        space_id = session.get('space_id')
        space_info = SpaceArea.query.filter_by(id=space_id).first()
        items = Objects.query.filter_by(space_area_id=space_id, model_files_id=model_select.id).all()
        class_link = ClassSpace.query.filter_by(space_id=space_id).first()
        if class_link:
            class_code = Classifications.query.filter_by(id=class_link.class_id).first()
        else:
            class_code = {'class_ref':' ', 'class_code':' '}
        if 'object_id' not in session:
            psets = None
        items =  Objects.query.filter_by(space_area_id=space_info.id, model_files_id=model_select.id).all()
        #print (items)
    else:
        space_info = SpaceArea.query.filter_by(model_files_id=model_select.id).first()
        class_code = {'class_ref':' ', 'class_code':' '}
        items = None
        psets = None
    if 'object_id' in session:
        object_id = session.get('object_id')
        object_info =  Objects.query.filter_by(id=object_id, model_files_id=model_select.id).first()
        psets = PropertySet.query.filter_by(object_id=object_id).all()
        object_class_link = ClassObject.query.filter_by(objects_id=object_id).first()
        if object_class_link:
            object_class_ref = Classifications.query.filter_by(id=object_class_link.class_id).first()
        else:
            object_class_ref = {'class_ref':' ', 'class_name':' '}
    else:
        object_info = []
        object_class_ref = {'class_ref':' ', 'class_name':' '}
    if 'property_set_id' in session:
        property_set_id = session.get('property_set_id')
        property_set_info = PropertySet.query.filter_by(id=property_set_id).first()
        properties = Property.query.filter_by(property_set_id=property_set_id)
    else:
        properties = []
    if form3.submit_room.data and form3.validate_on_submit():
        space_info = form3.room_list.data 
        class_link = ClassSpace.query.filter_by(space_id=space_info.id).first()
        if class_link:
            class_code = Classifications.query.filter_by(id=class_link.class_id).first()
        else:
            class_code = {'class_ref':' ', 'class_name':' '}
        items =  Objects.query.filter_by(space_area_id=space_info.id, model_files_id=model_select.id).all()

    if items:
        form4.object_list.query = items
        if form4.submit_object.data and form4.validate_on_submit():
            object_info = form4.object_list.data
            psets = PropertySet.query.filter_by(object_id=form4.object_list.data.id).all()
            object_class_link = ClassObject.query.filter_by(objects_id=object_info.id).first()
            if object_class_link:
                object_class_ref = Classifications.query.filter_by(id=object_class_link.class_id).first()
            else:
                object_class_ref = {'class_ref':' ', 'class_name':' '}
            new_items.extend(psets)
    if psets:
        form5.property_list.query = psets
        if form5.submit_property.data and form5.validate_on_submit():
            property_set_info = form5.property_list.data
            print (property_set_info.id)
            properties = Property.query.filter_by(property_set_id=property_set_info.id).all()
            temp = Property.query.filter_by(user_id=current_user.id).all()
            print (temp)
            for item in temp:
                print (item.property_set_id)
        #print (properties)
    if space_info:
        session['space_id'] = space_info.id
    if object_info:
        session['object_id'] = object_info.id
    if properties:
        session['property_set_id'] = property_set_info.id
    return render_template('select.html', title='Select', form3=form3, form4=form4, form5=form5, current_model=model_select, 
        space_info=space_info, class_code=class_code, items=items, new_items=new_items, object_info=object_info, 
        object_class_ref=object_class_ref, properties=properties, psets=psets)

@app.route('/classes', methods=['GET', 'POST'])
@login_required
def classes():
    services = []
    #form = SelectService(request.form)
    classes = Classifications.query.all()
    #classes = Classifications.class_ref.distinct().label("class_ref")
    #classes = [row.class_ref for row in query.all()]
    form2 = PopClass()    
    #This is only here as a temporary approach to load the classifications to the database
    if form2.submit_pop_class.data and form2.validate_on_submit():
        flash('classes added')
        uniclass_class = uniclass_lookup.class_list()
        #print (uniclass_class)
        
        for ind_class in uniclass_class:
            print (ind_class)
            try:
                full_class = Classifications(class_ref=ind_class[0], class_name=ind_class[5])
                db.session.add(full_class)
            except:
                pass
        db.session.commit()

    return render_template('classes.html', title='Select', form2=form2, services=services, classes=classes)


@app.route('/wall_quants', methods=['GET', 'POST'])
@login_required
def wall_quant():
    services = []
    form1 = ModelAndClass()

    if form1.validate_on_submit():
        flash('Classification {}'.format(
            form1.classification.data))
        flash('Model {}'.format(
            form1.model.data))
        wall_data, total_area = Measures.face_area(
            'IfcWall', form1.classification.data, form1.model.data)
        wall_data = jsonify(wall_data)
        return render_template('wall_quants.html', title='Wall Quantities', wall_data=wall_data, total_area=total_area)
    # if request.data:
    #    return render_template('wall_quants.html', title='Wall Quantities', wall_data=wall_data, total_area=total_area, message='hello')
    # else:
    #    return render_template('wall_quants.html', title='Wall Quantities', wall_data=wall_data, total_area=total_area, message='hello')
    return render_template('wall_selection.html', title='Select', form=form1, services=services)


@app.route('/door_quants', methods=['GET', 'POST'])
@login_required
def door_quant():
    services = []
    form1 = ModelAndClass()

    if form1.validate_on_submit():
        flash('Classification {}'.format(
            form1.classification.data))
        flash('Model {}'.format(
            form1.model.data))
        wall_data, total_area = Measures.door_data(
            'IfcDoor', form1.classification.data, form1.model.data)
        wall_data = jsonify(wall_data)
        return render_template('Door_quants.html', title='Doors', wall_data=wall_data, total_area=total_area)
    # if request.data:
    #    return render_template('wall_quants.html', title='Wall Quantities', wall_data=wall_data, total_area=total_area, message='hello')
    # else:
    #    return render_template('wall_quants.html', title='Wall Quantities', wall_data=wall_data, total_area=total_area, message='hello')
    return render_template('door_selection.html', title='Select', form=form1, services=services)'''


