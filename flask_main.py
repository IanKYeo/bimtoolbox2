# -*- coding: utf-8 -*-
"""
Created on Mon May 21 18:42:00 2018

@author: User
"""


from app import app, db
from app.models import User

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User}
